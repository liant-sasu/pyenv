# frozen_string_literal: true

require 'psych'

# Get your images
IMAGE_DESCRIPTIONS = Psych.safe_load_file('images.yml')
