# frozen_string_literal: true

require 'pathname'
require 'English'

needed_variables = %w[
  CI_PROJECT_NAME CI_COMMIT_REF_SLUG CI_DEFAULT_BRANCH CI_COMMIT_SHORT_SHA
  CI_REGISTRY CI_REGISTRY_USER CI_REGISTRY_PASSWORD CI_REGISTRY_PREFIX
]
alternative_variables = [%w[CI_COMMIT_BRANCH CI_COMMIT_TAG]]
unless needed_variables.all? { |variable| ENV.include?(variable) }
  warn 'Running outside Gitlab CI/CD Pipeline.'
  warn 'It can work for your tests, but you have to define at least those environment variables:'
  needed_variables.reject { |v| ENV.include?(v) }.each do |variable|
    warn "  - #{variable}"
  end
  exit 1
end

unless alternative_variables.all? { |couple| couple.any? { |variable| ENV.include?(variable) } }
  warn 'Running outside Gitlab CI/CD Pipeline.'
  warn 'It can work for your tests, but you have to define following environment variables:'
  alternative_variables.reject { |cpl| cpl.any? { |v| ENV.include?(v) } }.each do |couple|
    warn "  - One of the following: #{couple.join(' or ')}"
  end
  exit 1
end

DEBUG_LEVEL = ENV.fetch('DOCKER_DEBUG', '-q')

# run a command with some log separator and status report.
def run_system(name, cmds = nil, &block)
  puts '---------------'
  cmds&.each do |cmd|
    # puts "- cmd: #{cmd}"
    system(cmd)
    puts "- #{name} status: #{$CHILD_STATUS.success? ? 'OK' : 'FAILED'}"
  end
  unless block.nil?
    yield
    puts "- #{name} status: #{$CHILD_STATUS.success? ? 'OK' : 'FAILED'}"
  end
  puts '---------------'
  exit 1 unless $CHILD_STATUS.success?
end

def build_image_name
  if ENV['CI_COMMIT_TAG'].nil? && ENV['CI_COMMIT_BRANCH'] != ENV['CI_DEFAULT_BRANCH']
    "#{ENV['CI_PROJECT_NAME']}/#{ENV['CI_COMMIT_REF_SLUG']}"
  else
    ENV['CI_PROJECT_NAME']
  end
end

def build_base_tag(image_name, image_tag)
  "#{image_name}-#{image_tag}"
end

# Get the reference and push destinations
def tag_params(base_tag, prefix = '', suffix = '')
  # See https://docs.gitlab.com/ee/ci/variables/predefined_variables.html for details
  # Here we build a push depending on the situation of your pipeline.
  # if we are on the default branch but not on a tag ($CI_COMMIT_TAG == ''):
  #   - $CI_PROJECT_NAME:<base_tag>-latest
  #   - $CI_PROJECT_NAME:<base_tag>-$CI_COMMIT_SHORT_SHA
  # if we are on an other branch
  #   - $CI_PROJECT_NAME/$CI_COMMIT_REF_SLUG:<base_tag>-latest
  #   - $CI_PROJECT_NAME/$CI_COMMIT_REF_SLUG:<base_tag>-$CI_COMMIT_SHORT_SHA
  # if we are on a tag pipeline (then CI_COMMIT_REF_NAME == CI_COMMIT_TAG):
  #   - $CI_PROJECT_NAME:<base_tag>-$CI_COMMIT_REF_SLUG
  # Where base_tag is built with the method build_base_tag are the name and tag of the base image.

  dest_image_name = prefix == '' ? build_image_name : "#{prefix}/#{build_image_name}"
  dest_image_name = "#{dest_image_name}/#{suffix}" unless suffix == ''
  if ENV['CI_COMMIT_BRANCH'] == ENV['CI_DEFAULT_BRANCH'] || ENV['CI_COMMIT_TAG'].nil?
    tag_list = ["#{dest_image_name}:#{base_tag}-#{ENV['CI_COMMIT_SHORT_SHA']}"]
    tag_list.append("#{dest_image_name}:#{base_tag}-latest") if suffix == ''
    tag_list
  else
    ["#{dest_image_name}:#{base_tag}-#{ENV['CI_COMMIT_TAG']}"]
  end
end

# Get your Dockerfiles
DOCKERFILES = Pathname.glob('Dockerfile*')

# determine if we need to manually set the image platform by hand
def build_by_platforms_suffix(platforms)
  platforms_renamed = platforms.map { |plt| plt.gsub(/\//, '_') }
  platforms_renamed.join('-')
end

# Function to generate the build command
def build_cmd(image, tag, platforms, prefix, suffix)
  base_tag = build_base_tag(image, tag)
  dockerfile = find_dockerfile(image)
  "docker buildx build #{DEBUG_LEVEL} --push" \
  " --build-arg=BASE_IMAGE_NAME=#{image} --build-arg=BASE_IMAGE_TAG=#{tag}" \
  " -t #{tag_params(base_tag, prefix, suffix).join(' -t ')}" \
  " --platform='#{platforms.join(',')}' --file=#{dockerfile} ."
end

# Function to find the better suited Dockerfile for your image
def find_dockerfile(image)
  result = DOCKERFILES.find do |dockerfile|
    dockerfile.to_s == "Dockerfile.#{image}"
  end
  return result unless result.nil?

  Pathname('Dockerfile')
end
