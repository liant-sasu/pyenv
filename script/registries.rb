# frozen_string_literal: true

require_relative 'tools'

REGISTRIES = ENV.fetch('REGISTRIES', 'CI_REGISTRY')

def check_needed_variables(registry)
  needed_variables = ['', '_USER', '_PASSWORD', '_PREFIX'].map { |suffix| registry + suffix }
  unless needed_variables.all? { |variable| ENV.include?(variable) }
    warn "The following variables are missing to treat this registry (#{registry}):"
    needed_variables.reject { |v| ENV.include?(v) }.each do |variable|
      warn "  - #{variable}"
    end
    exit 1
  end
  needed_variables
end

def docker_login_on(registry)
  variables = check_needed_variables(registry)
  system("echo $#{variables[2]} | docker login -u $#{variables[1]} --password-stdin $#{variables[0]}")
end

def regctl_login_on(registry)
  variables = check_needed_variables(registry)
  system("echo $#{variables[2]} | regctl registry login $#{variables[0]} -u $#{variables[1]} --pass-stdin")
end

def path_to_image_for(registry)
  variables = check_needed_variables(registry)
  # $<registry>         $<registry>_PREFIX
  "#{ENV[variables[0]]}/#{ENV[variables[3]]}"
end

def docker_push_on(registry)
  system("docker push #{DEBUG_LEVEL} #{path_to_image_for(registry)}/#{build_image_name} -a")
end
