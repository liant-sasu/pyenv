#! /usr/bin/env ruby
# frozen_string_literal: true

require_relative 'tools'
require_relative 'images'
require_relative 'registries'

needed_variables = %w[
  CI_REGISTRY CI_REGISTRY_PREFIX CI_REGISTRY_PASSWORD CI_REGISTRY_USER
]
unless needed_variables.all? { |variable| ENV.include?(variable) }
  warn 'Running outside Gitlab CI/CD Pipeline.'
  warn 'It can work for your tests, but you have to define at least those environment variables:'
  needed_variables.reject { |v| ENV.include?(v) }.each do |variable|
    warn "  - #{variable}"
  end
  exit 1
end

# First get back the images
run_system('Login to Gitlab CI registry') do
  regctl_login_on('CI_REGISTRY')
  docker_login_on('CI_REGISTRY')
end

if ENV['REGISTRIES'].nil?
  warn 'Please define REGISTRIES where to push allow with their variables (see gitlab-ci.yml and README file)'
  exit 1
end

# Merge platforms manifests
if ENV['BUILT_PLATFORMS_LIST'].nil?
  warn 'Please define BUILD_PLATFORMS_LIST to let the script understands how you built your images'
  exit 1
end

IMAGE_DESCRIPTIONS.each do |image, tags|
  puts '---------------'
  puts "#{image}:"

  tags.each_key do |tag|
    puts '---------------'
    puts "  #{tag}:"
    puts '---------------'

    base_tag = build_base_tag(image, tag)

    # build the destination tag
    merge_manifest_tags = tag_params(base_tag, path_to_image_for('CI_REGISTRY'))
    source_manifest_tags = []
    ENV['BUILT_PLATFORMS_LIST'].split(' ').each do |built_platforms|
      platforms = built_platforms.split(',')
      platforms.sort!
      source_manifest_tags.append(
        tag_params(base_tag, path_to_image_for('CI_REGISTRY'), build_by_platforms_suffix(platforms))
      )
    end
    run_system(
      'Merge manifests',
      ["docker buildx imagetools create -t #{merge_manifest_tags.join(' -t ')} #{source_manifest_tags.join(' ')}"]
    )
  end
end

ENV['REGISTRIES'].split(' ').each do |registry_name|
  run_system("Login to '#{registry_name}' registry") do
    regctl_login_on(registry_name)
  end

  IMAGE_DESCRIPTIONS.each do |image, tags|
    puts '---------------'
    puts "#{image}:"

    tags.each_key do |tag|
      puts '---------------'
      puts "  #{tag}:"
      puts '---------------'

      base_tag = build_base_tag(image, tag)

      # retag from CI_REGISTRY to destination registry
      local_images = tag_params(base_tag, path_to_image_for('CI_REGISTRY'))
      cmds = tag_params(base_tag, path_to_image_for(registry_name)).map.each_with_index do |ci_image, i|
        "regctl image copy #{local_images[i]} #{ci_image}"
      end
      run_system('Copy images', cmds)
    end
  end
end
