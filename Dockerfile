ARG BASE_IMAGE_NAME
ARG BASE_IMAGE_TAG

FROM $BASE_IMAGE_NAME:$BASE_IMAGE_TAG

ARG BASE_IMAGE_NAME
ARG BASE_IMAGE_TAG

ENV NAME=$BASE_IMAGE_NAME:$BASE_IMAGE_TAG

# Install necessary packages
RUN case $BASE_IMAGE_TAG in \
    *alpine*) \
      apk add --no-cache curl git bash alpine-sdk build-base \
      libffi-dev openssl-dev bzip2-dev zlib-dev xz-dev readline-dev sqlite-dev tk-dev \
      ;; \
    *bookworm) \
      apt-get update -qq \
      && apt-get install -qqyy curl git build-essential \
        libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev curl git \
        libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev \
      ;; \
    esac

ENV HOME=/root

# Move to user home directory
WORKDIR $HOME

# Define Pyenv environment variables
ENV PYENV_ROOT=$HOME/.pyenv
ENV PATH=$PYENV_ROOT/shims:$PYENV_ROOT/bin:$PATH

# Install pyenv
RUN curl https://pyenv.run | bash

# Install python 3.9
RUN pyenv install 3.9

# Install python 3.10
RUN pyenv install 3.10

# Install python 3.11
RUN pyenv install 3.11

# Install python 3.12
RUN pyenv install 3.12

# Clean package manager
RUN case $BASE_IMAGE_TAG in \
    *alpine) \
      echo "Nothing to do on Alpine";; \
    *debian) \
      apt-get clean \
    ;; \
    esac
