# Pyenv Image

This project is simply to prepare an image with pyenv ready to use.
This is useful for Gitlab CI or other CI/CD tools.

## Reference links

- [GitLab CI Documentation](https://docs.gitlab.com/ee/ci/)
- [Docker](https://www.docker.com)
- [Pyenv](https://github.com/pyenv/pyenv)

If you are new to docker, please use [stack_overflow](https://stackoverflow.com) and
Docker's documentation.

If you are new to Pyenv, please use [stack_overflow](https://stackoverflow.com) and
[Pyenv's documentation](https://github.com/pyenv/pyenv/blob/master/COMMANDS.md).

## What's contained in this project

We provide a simple docker image running on several architectures and with severals
linux distribution (alpine and debian 12 slim).

We also already built Python 3.9 to 3.12.

Environment variables PYENV_HOME is defined and PATH access to pyenv built commands.

A user (not root) has been created and you run as him in its home directory.

## Usage

To use this, in Gitlab for a test environment for example, do the following
(`.gitlab-ci.yml`):

```yaml
test:
  stage: test
  image: rlaures/pyenv:python-3.12-alpine3.20-latest
  coverage: /(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/
  before_script:
    - pyenv local 3.12 3.11 3.10 3.9
    - pyenv shims
    - pip install tox
  script:
    - tox
  artifacts:
    when: always
    paths:
      - coverage.xml
      - .coverage
      - report.xml
    reports:
      junit: report.xml
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
```

### Docker HUB

To deploy on Docker Hub, this will be done only on `deploy` job and if DOCKER_TOKEN,
DOCKER_BASE_NAME (basically where your token has the right to deploy) environment
variables are set.

## Support

First get support on [stack_overflow](https://stackoverflow.com), you can tag
me [@rlaures](https://stackoverflow.com/users/3129859/rlaures) or recruit me or
my team.

## Need more images or signal a bug/flaw

Use the [issue tracking system of Gitlab](https://gitlab.com/liant-sasu/templates/docker-template/-/issues) please.
Follow the contribution code and please be cordial.

## Contributing

See [Contribution file](./CONTRIBUTING.md) to contribute (basically, create an issue
[here](https://gitlab.com/liant-sasu/docker-template/-/issues/new) and be clear).

## Authors and acknowledgment

Contributors are from [Liant](https://gitlab.com/liant-sasu) (at this moment).

## License

[MIT](./LICENSE)
